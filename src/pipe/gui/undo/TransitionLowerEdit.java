package pipe.gui.undo;

import pipe.dataLayer.Transition;

/**
*
* @author Lingbo
*/
public class TransitionLowerEdit
		extends UndoableEdit {
	Transition transition;
	double oldLower;
	double newLower;
	
	   public TransitionLowerEdit(
	           Transition _transition, double _oldLower, double _newLower) {
	      transition = _transition;
	      oldLower = _oldLower;      
	      newLower = _newLower;
	   }

	   
	   /** */
	   public void undo() {
		   transition.setLower(oldLower);
	   }

	   
	   /** */
	   public void redo() {
		   transition.setLower(newLower);
	   }
}
