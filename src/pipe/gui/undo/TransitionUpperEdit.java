package pipe.gui.undo;

import pipe.dataLayer.Transition;

/**
*
* @author Lingbo
*/
public class TransitionUpperEdit
		extends UndoableEdit {
	Transition transition;
	double oldUpper;
	double newUpper;
	
	   public TransitionUpperEdit(
	           Transition _transition, double _oldUpper, double _newUpper) {
	      transition = _transition;
	      oldUpper = _oldUpper;      
	      newUpper = _newUpper;
	   }

	   
	   /** */
	   public void undo() {
		   transition.setUpper(oldUpper);
	   }

	   
	   /** */
	   public void redo() {
		   transition.setUpper(newUpper);
	   }
}
